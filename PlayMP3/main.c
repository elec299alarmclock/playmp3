//This program uses the RTC to display time on a 4 digit 7 segment display
//When the alarm triggers, it plays mp3 files through a USB connected on the
//micro USB port

#include "stm32f4xx_rtc.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_pwr.h"
#include "stm32f4xx_dac.h"
#include "stm32f4xx_tim.h"
#include "misc.h"
#include "stm32f4xx_exti.h"
#include "audioMP3.h"
#include "main.h"
#include "Audio.h"
#include "stm32f4xx_spi.h"
#include "stm32f4xx_i2c.h"
#include "stm32f4xx_dac.h"

//structures
RCC_ClocksTypeDef RCC_Clocks;
GPIO_InitTypeDef	GPIOInitStruct;
TIM_TimeBaseInitTypeDef TIM_InitStruct;
NVIC_InitTypeDef NVIC_InitStructure;
EXTI_InitTypeDef EXTI_InitStructure;


//function prototypes
void configuration(void);
void increase_minutes(int minutes, RTC_TimeTypeDef * target_time); //Increase time in 24 H
void decrease_minutes(int minutes, RTC_TimeTypeDef * target_time); //Decrease time in 24 H
void change24to12(RTC_TimeTypeDef * target_time); //Change from 24H clock to 12H
void change12to24(RTC_TimeTypeDef * target_time); //Change from 12H clock to 24H
void change_format(bool format24); //Change all formats for all times
void LED_display(); //LED functions
void default_state(); //Default state for setting time
void setting_alarm_state(); //State for setting alarm
void setting_volume_state(); //State for setting alarm
void clear_variables(); //Clears variables in between states
void load_time_to_array(RTC_TimeTypeDef target_time); //load time into array to display LED
//global variables
int interruptOccurred = 0;
int timechange = 1;
unsigned char pinChoice[5] = {1,2,0,0,10}; //Numbers are cases, pinChoice[4] is colon
unsigned char counterpin = 0; //Selects which digit to display, loops through all of them
extern volatile int exitMp3 = 0; //Exit loop of playing
extern volatile int mp3PlayingFlag = 0;
extern volatile int snoozeMemory = 0;
extern volatile int pause_flag = 0;
int minutes = 0,alarm_counter = 0,volume_counter = 0; //Variables for buttons
int16_t volume  = 20; //Volume 1 to 20
bool button_time_format = false,button_alarm = false,time_format24 = true,alarm_a_on = false,button_volume = false; //Button states,checks if pressed

void (*execute_state[3])() = {default_state,setting_alarm_state,setting_volume_state}; //All possible states
unsigned char current_state = 0; //Current state of execution

RTC_TimeTypeDef current_time; //Current time to display
RTC_TimeTypeDef alarm_time; //Time of alarm se to play music

/*
 * ALARM CLOCK BUTTONS
 * PC4 and PC5 are for increasing and decreasing
 * PC9 is for turning on/off and setting alarm
 * PC8 is for 24H/12H and volume control
 *
 * Features
 * Acceleration for setting time
 * Snooze when pressing PC9
 * Turn off and on using PC9
 * Hold PC9 to set alarm (FLASHES)
 * Press PC8 for 12H/24H
 * Alarm works in 12H/24H
 * Hold PC8 for volume control
 * Volume control works in real time
 * Turns off music after playing one music file
 * Used software debouncing
 *
 */



void clear_variables()
{
	button_time_format = false;
	minutes = 0;
	alarm_counter = 0;
	volume_counter = 0;
}

int main(void)
{

  configuration();

  while ( 1 )
  {
	  if (mp3PlayingFlag == 1)
	  {
		  audioToMp3();
	  }
  }
}

//Target time is the time that will be changed
void change12to24(RTC_TimeTypeDef * target_time)
{
	if ((target_time->RTC_Hours == 12) && ((target_time->RTC_H12 == RTC_H12_AM) && (target_time->RTC_Minutes == 0)))
	{
		target_time->RTC_Hours = 0;
	}
	else if ((target_time->RTC_Hours == 12) && ((target_time->RTC_H12 == RTC_H12_PM) && (target_time->RTC_Minutes == 0)))
	{
		target_time->RTC_Hours = 12;
	}
	else if ((target_time->RTC_H12 == RTC_H12_AM) && (target_time->RTC_Hours == 12))
	{
		target_time->RTC_Hours = 0;
	}
	else if ((target_time->RTC_H12 == RTC_H12_PM) && (target_time->RTC_Hours < 12))
	{
		target_time->RTC_Hours += 12;
	}
	target_time->RTC_H12 = RTC_H12_AM;
}


void change24to12(RTC_TimeTypeDef * target_time)
{
	if ((target_time->RTC_Hours == 0) && (target_time->RTC_Minutes == 0))
	{
		target_time->RTC_Hours = 12;
		target_time->RTC_H12 = RTC_H12_AM;
	}
	else if ((target_time->RTC_Hours == 12)  && (target_time->RTC_Minutes == 0))
	{
		target_time->RTC_Hours = 12;
		target_time->RTC_H12 = RTC_H12_PM;
	}
	else if (target_time->RTC_Hours < 12)
	{
		if (target_time->RTC_Hours == 0)
		{
			target_time->RTC_Hours = 12;
		}
		target_time->RTC_H12 = RTC_H12_AM;
	}
	else if  (target_time->RTC_Hours >= 12)
	{
		if (target_time->RTC_Hours > 12)
		{
			target_time->RTC_Hours -= 12;
		}
		target_time->RTC_H12 = RTC_H12_PM;
	}
}



void change_format(bool format24)
{

	if (format24)
	{
		//Set 24H
		RTC_InitTypeDef myclockInitTypeStruct;
		myclockInitTypeStruct.RTC_HourFormat = RTC_HourFormat_24;
		myclockInitTypeStruct.RTC_AsynchPrediv = 127;
		myclockInitTypeStruct.RTC_SynchPrediv = 0x00FF;
		RTC_Init(&myclockInitTypeStruct);
		change12to24(&current_time);
		change12to24(&alarm_time);
	}
	else
	{
		//Set 12H
		RTC_InitTypeDef myclockInitTypeStruct;
		myclockInitTypeStruct.RTC_HourFormat = RTC_HourFormat_12;
		myclockInitTypeStruct.RTC_AsynchPrediv = 127;
		myclockInitTypeStruct.RTC_SynchPrediv = 0x00FF;
		RTC_Init(&myclockInitTypeStruct);
		change24to12(&current_time);
		change24to12(&alarm_time);
	}

	RTC_SetTime(RTC_Format_BIN,&current_time);
	//Enable or disable alarm
	if (alarm_a_on)
	{
		RTC_AlarmCmd(RTC_Alarm_A,DISABLE);
		RTC_SetAlarm(RTC_Format_BIN,RTC_Alarm_A,&alarm_time);
		RTC_AlarmCmd(RTC_Alarm_A,ENABLE);
	}
	else
	{
		RTC_SetAlarm(RTC_Format_BIN,RTC_Alarm_A,&alarm_time);
		RTC_AlarmCmd(RTC_Alarm_A,DISABLE);
	}


}



void increase_minutes(int minutes, RTC_TimeTypeDef * target_time)
{
	target_time->RTC_Minutes += minutes; //Add minutes
	if (target_time->RTC_Minutes >= 60)
	{
		target_time->RTC_Hours += 1;
		target_time->RTC_Minutes %= 60;
	}
	//Account for hours rolling over
	if (target_time->RTC_Hours >= 24)
	{
		target_time->RTC_Hours %= 24;
	}
}


void decrease_minutes(int minutes, RTC_TimeTypeDef * target_time)
{
	int hours = target_time->RTC_Hours;
	if ((target_time->RTC_Minutes - minutes) < 0)
	{
		hours -= 1;
		target_time->RTC_Minutes = 60-minutes;
	}
	else
	{
		target_time->RTC_Minutes -= minutes;
	}

	if (hours < 0)
	{
		hours = 24-1;
	}
	target_time->RTC_Hours = hours;
}

//Loads RTC time to LED display function
void load_time_to_array(RTC_TimeTypeDef target_time)
{
	if (target_time.RTC_H12 == RTC_H12_PM)
	{
		pinChoice[4] = 8; //Turn on PM LED
	}
	else
	{
		pinChoice[4] = 10;
	}

	pinChoice[0] = target_time.RTC_Hours /10;
	pinChoice[1] = target_time.RTC_Hours %10;
	pinChoice[2] = target_time.RTC_Minutes /10;
	pinChoice[3] = (target_time.RTC_Minutes) %10;
}




void LED_display()
{
	//Turn off all pins
	GPIO_ResetBits(GPIOD, GPIO_Pin_7| GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11);
	GPIO_ResetBits(GPIOE, GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9| GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15);

	//Select from one of the 5 cases.
	unsigned char multi = (counterpin % 5);

	/* Digits configured to GPIO pins D(7-11); case 4 is the semicolon on the LED display.
	Case 0: Digit 1 - D7
	Case 1: Digit 2 - D8
	Case 2: Digit 3 - D9
	Case 3: Digit 4 - D10
	Case 4: Semicolon - D11
	*/

	if ((current_state != 1) || (counterpin < 128) ) //Flash when setting alarm tme
	{
		//Select digit
		switch (multi)
		{
			case 0:
					if (current_state != 2) //Turn off the setting volume
					{
						GPIO_SetBits(GPIOD, GPIO_Pin_7);
					}

					break;
			case 1:
					if (current_state != 2) //Turn off the setting volume
					{
						GPIO_SetBits(GPIOD, GPIO_Pin_8);
					}

					break;
			case 2: GPIO_SetBits(GPIOD, GPIO_Pin_9);
					break;
			case 3: GPIO_SetBits(GPIOD, GPIO_Pin_10);
					if (alarm_a_on) //Turn on light for alarm
					{
						GPIO_SetBits(GPIOE, GPIO_Pin_13 );
					}

					break;
			case 4: GPIO_SetBits(GPIOD, GPIO_Pin_11);
					break;
		}
	}
	// Displaying time based on pin selection.

	/* Selects correct segments based on the digit value configured to pins E(6-12) for values (0-9)
	Segment:	Value:
	a - E6		0 - a, b, c, d, e, f
	b - E7		1 - b, c
	c - E8		2 - a, b, d, e, g
	d - E9		3 - a, b, c, d, g
	e - E10		4 - b, c, f, g
	f - E11		5 - a, c, d, f, g
	g - E12		6 - a, c, d, e, f, g
	k - E13		7 - a, b, c
				8 - a, b, c, d, e, f
				9 - a, b, c, d, f, g

	Display:
	Top (Left -> Right)
	| 1 - PCB Mid 2 | 2 - PCB Mid 7 | 3 - PCB Mid 1 | 4 - PCB Mid 3 | 5 - PCB Mid 6 |

	Bottom (Left -> Right)
	| 1 - PCB Mid 9 | 2 - PCB Mid 10 | 3 - PCB Mid 4 | 4 - PCB Mid 13 | 5 - PCB Mid 5 | 6 - PCB Mid 11 | 7 - PCB Mid (N/A)
	| 8 - PCB Mid 12 |


	PCB:

	| 1 - PCB Left E6| 2 - PCB Left E7 | 3 - PCB Left E8 | 4 - PCB Left E9 | 5 - PCB Left E10 | 6 - PCB Left E11
	| 7 - PCB Left E12| 9 - PCB Left D7 | 10 - PCB Left D8 | 11 - PCB Left D9 | 12 - PCB Left D10 | 12 - PCB Left D11 |

	*/
	switch (pinChoice[multi])
	{
		case 0:
			GPIO_SetBits(GPIOE, GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11);
			break;
		case 1:
			GPIO_SetBits(GPIOE,  GPIO_Pin_7 | GPIO_Pin_8 );
			break;
		case 2:
			GPIO_SetBits(GPIOE,  GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_12 );
			break;
		case 3:
			GPIO_SetBits(GPIOE,  GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_12 );
			break;
		case 4:
			GPIO_SetBits(GPIOE,  GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_11 | GPIO_Pin_12 );
			break;
		case 5:
			GPIO_SetBits(GPIOE, GPIO_Pin_6 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_11 | GPIO_Pin_12);
			break;
		case 6:
			GPIO_SetBits(GPIOE, GPIO_Pin_6 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12);
			break;
		case 7:
			GPIO_SetBits(GPIOE, GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 );
			break;
		case 8:
			GPIO_SetBits(GPIOE, GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12);
			break;
		case 9:
			GPIO_SetBits(GPIOE, GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_11 | GPIO_Pin_12);
			break;
		case 10:
			GPIO_SetBits(GPIOE, GPIO_Pin_6 | GPIO_Pin_7 );
			break;
	}

}



void default_state()
{
	//Get current time
	RTC_GetTime(RTC_Format_BIN,&current_time);
	//Display current time
	load_time_to_array(current_time);

	//Increase minutes
	if ((GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_4) >= 1)){
		if (minutes < 15)
		{
			++minutes;
		}

		//Disable alarm
		RTC_AlarmCmd(RTC_Alarm_A,DISABLE);
		//Select 24H or 12H
		if (time_format24)
		{
			increase_minutes(minutes,&current_time);
		}
		else
		{
			change12to24(&current_time);
			increase_minutes(minutes,&current_time);
			change24to12(&current_time);
		}

		RTC_SetTime(RTC_Format_BIN,&current_time);

		//Enable alarm
		if (alarm_a_on)
		{
			RTC_AlarmCmd(RTC_Alarm_A,ENABLE);
		}
		else
		{
			RTC_AlarmCmd(RTC_Alarm_A,DISABLE);
		}

	}//Decrease minutes
	else if ((GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_5) >= 1)){
		if (minutes < 15)
		{
			++minutes;
		}
		//Disable alarm
		RTC_AlarmCmd(RTC_Alarm_A,DISABLE);
		//Select 24H or 12H
		if (time_format24)
		{
			decrease_minutes(minutes,&current_time);
		}
		else
		{
			change12to24(&current_time);
			decrease_minutes(minutes,&current_time);
			change24to12(&current_time);
		}

		RTC_SetTime(RTC_Format_BIN,&current_time);

		//Enable alarm
		if (alarm_a_on)
		{
			RTC_AlarmCmd(RTC_Alarm_A,ENABLE);
		}
		else
		{
			RTC_AlarmCmd(RTC_Alarm_A,DISABLE);
		}

	}
	else
	{
		minutes = 0;
	}

	//Set AM/PM or volume
	if (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_8) >= 1) {
		if (volume_counter < 15)
		{
			++volume_counter;
		}

		if (volume_counter == 15)
		{
			//Go to setting volume state
			current_state = 2;
			clear_variables();
		}
	}
	else
	{
		//Change AM/PM
		if (volume_counter > 0)
		{
			if (time_format24)
			{
				time_format24 = false;
			}
			else
			{
				time_format24 = true;
			}
			//Change all times
			change_format(time_format24);
		}
		volume_counter = 0;
	}


	//Set alarm or turn on/off
	if (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_9) >= 1) {
		if (alarm_counter < 15)
		{
			++alarm_counter;
		}

		//Go to setting alarm state
		if (alarm_counter == 15)
		{
			RTC_AlarmCmd(RTC_Alarm_A,DISABLE);
			current_state = 1;
			clear_variables();
		}
	}
	else
	{
		//Turn off/on alarm
		if (alarm_counter > 0)
		{
			if (alarm_a_on)
			{
				alarm_a_on = false;
				RTC_AlarmCmd(RTC_Alarm_A,DISABLE);
				exitMp3 = 1;
			}
			else
			{
				alarm_a_on = true;
				RTC_AlarmCmd(RTC_Alarm_A,ENABLE);
				exitMp3 = 0;
			}

		}
		alarm_counter = 0;
	}

}


void setting_alarm_state()
{
	//Get alarm time
	RTC_GetAlarm(RTC_Format_BIN,RTC_Alarm_A,&alarm_time);
	//Display alarm time
	load_time_to_array(alarm_time);

	//Increase minutes
	if ((GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_4) >= 1)){
		//Accelerate until 15
		if (minutes < 15)
		{
			++minutes;
		}

		//Select 24h or 12H
		if (time_format24)
		{
			increase_minutes(minutes,&alarm_time);
		}
		else
		{
			change12to24(&alarm_time);
			increase_minutes(minutes,&alarm_time);
			change24to12(&alarm_time);
		}

		RTC_SetAlarm(RTC_Format_BIN,RTC_Alarm_A,&alarm_time);
		RTC_AlarmCmd(RTC_Alarm_A,DISABLE);
	}//Decrease minutes
	else if ((GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_5) >= 1)){
		//Accelerate until 15
		if (minutes < 15)
		{
			++minutes;
		}

		//Select 24h or 12H
		if (time_format24)
		{
			decrease_minutes(minutes,&alarm_time);
		}
		else
		{
			change12to24(&alarm_time);
			decrease_minutes(minutes,&alarm_time);
			change24to12(&alarm_time);
		}

		RTC_SetAlarm(RTC_Format_BIN,RTC_Alarm_A,&alarm_time);
		RTC_AlarmCmd(RTC_Alarm_A,DISABLE);
	}
	else
	{
		minutes = 0;
	}

	//Go back to default state
	if (((GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_9) >= 1)) && (!button_alarm)){
			button_alarm = true;
			current_state = 0;
			//Disable
			RTC_AlarmCmd(RTC_Alarm_A,DISABLE);
			//Set alarm
			RTC_SetAlarm(RTC_Format_BIN,RTC_Alarm_A,&alarm_time);
			//Enable
			RTC_AlarmCmd(RTC_Alarm_A,ENABLE);
			clear_variables();
			alarm_a_on = true;
	}
	else if (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_9) == 0)
	{
		button_alarm = false;
	}
}


void setting_volume_state()
{
	//Display volume
	pinChoice[2] = volume /10;
	pinChoice[3] = volume %10;
	pinChoice[4] = 10;

	//Increase volume
	if ((GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_4) >= 1)){
		//Max of 20 volume
		if (volume < 20)
		{
			++volume;
		}

	}//Decrease volume
	else if ((GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_5) >= 1)){
		//Min of 1
		if (volume > 1)
		{
			--volume;
		}
	}

	//Go back to default state
	if (((GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_8) >= 1)) && (!button_volume)){
			button_volume = true;
			current_state = 0;
			clear_variables();
	}
	else if (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_8) == 0)
	{
		button_volume = false;
	}

}


//timer interrupt handler that is called at a rate of 500Hz
//this function gets the time and displays it on the 7 segment display
//it also checks for button presses, debounces, and handles each case
void TIM5_IRQHandler(void)
{
	int previousState = 0;

	//double checks that interrupt has occurred
	if( TIM_GetITStatus( TIM5, TIM_IT_Update ) != RESET )
	{
		// MAIN LOOP
		if ((counterpin % 50) == 0)
		{
			execute_state[current_state]();
		}
		
	    //Drive the LED
	    LED_display();
		//clears interrupt flag
	    TIM5->SR = (uint16_t)~TIM_IT_Update;
	    //Increment pin
	    counterpin++;
    }
}

//alarm A interrupt handler
//when alarm occurs, clear all the interrupt bits and flags
//then set the flag to play mp3
void RTC_Alarm_IRQHandler(void)
{

	//resets alarm flags and sets flag to play mp3
	if(RTC_GetITStatus(RTC_IT_ALRA) != RESET)
	{
		RTC_ClearFlag(RTC_FLAG_ALRAF);
		RTC_ClearITPendingBit(RTC_IT_ALRA);
		EXTI_ClearITPendingBit(EXTI_Line17);
		interruptOccurred = 1;
		mp3PlayingFlag = 1;

	}
}


//configures the clocks, gpio, alarm, interrupts etc.
void configuration(void)
{
	  //lets the system clocks be viewed
	  RCC_GetClocksFreq(&RCC_Clocks);

	  //enable peripheral clocks
	  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
	  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
	  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);

	  //enable the RTC
	  PWR_BackupAccessCmd(DISABLE);
	  PWR_BackupAccessCmd(ENABLE);
	  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
	  RCC_RTCCLKCmd(ENABLE);
	  RTC_AlarmCmd(RTC_Alarm_A,DISABLE);

	  //Enable the LSI OSC
	  RCC_LSICmd(ENABLE);

	  //Wait till LSI is ready
	  while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET);

	  //enable the external interrupt for the RTC to use the Alarm
	  /* EXTI configuration */
	  EXTI_ClearITPendingBit(EXTI_Line17);
	  EXTI_InitStructure.EXTI_Line = EXTI_Line17;
	  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	  EXTI_Init(&EXTI_InitStructure);

	  //set timer 5 to interrupt at a rate of 500Hz
	  TIM_TimeBaseStructInit(&TIM_InitStruct);
	  TIM_InitStruct.TIM_Period	=  8000;	// 500Hz
	  TIM_InitStruct.TIM_Prescaler = 20;
	  TIM_TimeBaseInit(TIM5, &TIM_InitStruct);

	  // Enable the TIM5 global Interrupt
	  NVIC_Init( &NVIC_InitStructure );
	  NVIC_InitStructure.NVIC_IRQChannel = TIM5_IRQn;
	  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
      NVIC_Init( &NVIC_InitStructure );

      //setup the RTC for 12 hour format
	  myclockInitTypeStruct.RTC_HourFormat = RTC_HourFormat_24;
	  myclockInitTypeStruct.RTC_AsynchPrediv = 127;
	  myclockInitTypeStruct.RTC_SynchPrediv = 0x00FF;
	  RTC_Init(&myclockInitTypeStruct);

	  //set the time displayed on power up to 12AM
	  myclockTimeStruct.RTC_H12 = RTC_H12_AM;
	  myclockTimeStruct.RTC_Hours = 0x12;
	  myclockTimeStruct.RTC_Minutes = 0x000;
	  myclockTimeStruct.RTC_Seconds = 0x000;
	  RTC_SetTime(RTC_Format_BCD, &myclockTimeStruct);


	  //sets alarmA for 12:00AM, date doesn't matter
	  AlarmStruct.RTC_AlarmTime.RTC_H12 = RTC_H12_AM;
	  AlarmStruct.RTC_AlarmTime.RTC_Hours = 0x12;
	  AlarmStruct.RTC_AlarmTime.RTC_Minutes = 0x00;
	  AlarmStruct.RTC_AlarmTime.RTC_Seconds = 0x00;
	  AlarmStruct.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay;
	  RTC_SetAlarm(RTC_Format_BCD,RTC_Alarm_A,&AlarmStruct);

	  // Enable the Alarm global Interrupt
	  NVIC_Init( &NVIC_InitStructure );
	  NVIC_InitStructure.NVIC_IRQChannel = RTC_Alarm_IRQn;
	  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	  NVIC_Init( &NVIC_InitStructure );

	  //IO for push buttons using internal pull-up resistors
	  GPIO_StructInit( &GPIOInitStruct );
	  GPIOInitStruct.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_11;
	  GPIOInitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	  GPIOInitStruct.GPIO_Mode = GPIO_Mode_IN;
	  GPIOInitStruct.GPIO_OType = GPIO_OType_PP;
	  GPIOInitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	  GPIO_Init(GPIOC, &GPIOInitStruct);

	  //configure GPIO for digits
	  GPIO_StructInit( &GPIOInitStruct );
	  GPIOInitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	  GPIOInitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	  GPIOInitStruct.GPIO_Mode = GPIO_Mode_OUT;
	  GPIOInitStruct.GPIO_OType = GPIO_OType_PP;
	  GPIOInitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	  GPIO_Init(GPIOE, &GPIOInitStruct);

	  //configure GPIO for multiplexing
	  GPIO_StructInit( &GPIOInitStruct );
	  GPIOInitStruct.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11;
	  GPIOInitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	  GPIOInitStruct.GPIO_Mode = GPIO_Mode_OUT;
	  GPIOInitStruct.GPIO_OType = GPIO_OType_PP;
	  GPIOInitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	  GPIO_Init(GPIOD, &GPIOInitStruct);

	  //enables RTC alarm A interrupt
	  RTC_ITConfig(RTC_IT_ALRA, ENABLE);

	  //enables timer interrupt
	  TIM5->DIER |= TIM_IT_Update;

	  //enables timer
	  TIM5->CR1 |= TIM_CR1_CEN;

	  //Get current alarm time
	  RTC_GetAlarm(RTC_Format_BIN,RTC_Alarm_A,&alarm_time);
	  //Set time
	  alarm_time.RTC_Hours = 12;
	  alarm_time.RTC_Minutes = 0;
	  alarm_time.RTC_Seconds = 0;
	  alarm_time.RTC_H12 = RTC_H12_AM;

	  //Place alarm time
	  RTC_AlarmCmd(RTC_Alarm_A,DISABLE);
	  RTC_SetAlarm(RTC_Format_BIN,RTC_Alarm_A,&alarm_time);
	  RTC_AlarmCmd(RTC_Alarm_A,DISABLE);

	  //mp3PlayingFlag = 1;
}


