void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  float i;
  unsigned int Count;
  
  while ( 1 == 1 )
  {
    i = 0;
    Count = 10;
    
    while ( Count > 0 )
    {
      Serial.print( "Value " );
      Serial.print( i, 3 );
      Serial.print( " has a square root of " );
      Serial.println( sqrt( i ), 3 );
      Count--;
      i = i + 10.0;
    }

    Serial.println( "" );
    delay( 5000 );
  }
}
